<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sunali's Classes is a tution based in Ahmedabad. We specialize in CBSE and NCERT teaching. The Smartest CBSE & NCERT  Tuitions in Ahmedabad">
    <meta name="author" content="Pixolo Webworks">

    <meta name="keywords" content="Best CBSE tuitions, Maths CBSE, Science CBSE, VIII tutorials, IX tutorials, X tutorials, NCERT classes, Physics, Chemistry, Biology, Maths, English, Social Studies, SS, S.S., History, Geography, Literature, English, Economics, 8, 9, 10, 8th, 9th , 10th, Doubts, Assignment, Smart, Smart Board, App, Education, Higher secondary, Ahmedabad, Gujarat, Classroom tuition">

    <title>Sunalis Classes - Blog</title>
    <!--Favicon-->
    <link rel="shortcut icon" href="http://sunalisclasses.com/img/favicon.png" type="image/x-icon">

    <!-- Bootstrap core CSS -->
    <link href="http://sunalisclasses.com/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="http://sunalisclasses.com/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://sunalisclasses.com/vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="http://sunalisclasses.com/device-mockups/device-mockups.min.css?v=2">

    <!-- Custom styles for this template -->
    <link href="http://localhost/sunalisclasses/css/new-age.css" rel="stylesheet">

    <!-- JQuery -->
    <script src="http://sunalisclasses.com/vendor/jquery/jquery.min.js"></script>

    <!--Animate Css -->
    <link href="http://sunalisclasses.com/vendor/animate/animate.css" rel="stylesheet">




    <style>
        .navbar-brand {
            display: inline-block;
            width: 50%;
        }

        .navbar-brand img {
            width: 30%;
        }

        @media screen and (max-width: 800px) {
            .navbar-brand img {
                width: 70%;
            }
        }

        input:focus {
            outline: 2px solid #32d3df !important;
        }

        textarea:focus {
            outline: 2px solid #32d3df;
        }



        .blog-container {
            background-color: #fff;

        }

        .pixolo {
            color: #00D2DF;
        }

        .copyright {
            color: #00D2DF;
        }

        .blog-solial-media-icons {
            display: flex;
            justify-content: center;
            flex-direction: column;
        }

        .blog-social-media-wrapper {
            width: 180px;
            text-align: center;
            padding-top: 15px;
        }

        .blog-solial-media-icons {
            position: sticky;
            top: 80px;
            padding-bottom: 20px;

        }

    </style>


    <?php
    $url_title = $_SERVER['REQUEST_URI'];
    $url_title =  explode("/",$url_title)[2];
//    $url_title = substr($url_title, 0, strlen($url_title)-5);
    
    $response= exec('curl https://sunalis-classes-website.firebaseio.com/blogs.json');
    
    // $url_title='PARIKSHA-PE-CHARCHA-WITH-PM';
    $blogs_data= json_decode($response, true);
    
      foreach($blogs_data as $blog){
    		if (strtolower($blog['url'])==$url_title){
    			$blog_to_show = $blog;
    		}
    	};
    ?>




</head>

<body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light" id="blogNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="http://sunalisclasses.com/img/logo.png" width="40%" alt="Sunalis Classes" /></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fa fa-bars"></i>
            </button>
            <a href="http://sunalisclasses.com/" class="navbar-go-back">GO BACK</a>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <!-- <ul class="navbar-nav ml-auto">
                    <li class="nav-item" style="min-width: 100px">
                        <a class="nav-link js-scroll-trigger" href="#aboutus">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#features">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#download">Revolution</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#leaderboard">Leaderboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#gallery">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#blogs">Blogs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                    </li>
                </ul> -->
            </div>

        </div>
    </nav>
    <script>
        $(document).ready(function() {
            var mobiles = ['iphone6_plus', 'galaxy_s5', 'lumia920'];
            var random = Math.floor(Math.random() * 3) + 1
            $('.device-mockup').addClass(mobiles[random - 1]);
        })

    </script>



    <section class="blog-post">
        <div class="container">
            <div class="row">
                <div class="column1 col-lg-9">
                    <div class="content-area">
                        <div class="blogpost-main-image" style="margin-top:50px;">
                            <img src="<?php echo $blog_to_show['image']; ?>" alt="Image">
                            <div class="blog-info-wrapper">
                                <h3 class="blog-title">
                                    <?php echo $blog_to_show['title']; ?>
                                </h3>
                                <div class="post-details">
                                    <div class="inner-detail-wrapper">
                                        <svg enable-background="new 0 0 60 60" version="1.1" viewBox="0 0 60 60" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M30,0C13.458,0,0,13.458,0,30s13.458,30,30,30s30-13.458,30-30S46.542,0,30,0z M30,58C14.561,58,2,45.439,2,30   S14.561,2,30,2s28,12.561,28,28S45.439,58,30,58z" />
                                            <path d="m31 26.021v-10.142c0-0.553-0.448-1-1-1s-1 0.447-1 1v10.142c-1.399 0.364-2.494 1.459-2.858 2.858h-7.142c-0.552 0-1 0.447-1 1s0.448 1 1 1h7.142c0.447 1.72 2 3 3.858 3 2.206 0 4-1.794 4-4 0-1.859-1.28-3.411-3-3.858zm-1 5.858c-1.103 0-2-0.897-2-2s0.897-2 2-2 2 0.897 2 2-0.897 2-2 2z" />
                                            <path d="m30 9.879c0.552 0 1-0.447 1-1v-1c0-0.553-0.448-1-1-1s-1 0.447-1 1v1c0 0.553 0.448 1 1 1z" />
                                            <path d="m30 49.879c-0.552 0-1 0.447-1 1v1c0 0.553 0.448 1 1 1s1-0.447 1-1v-1c0-0.553-0.448-1-1-1z" />
                                            <path d="m52 28.879h-1c-0.552 0-1 0.447-1 1s0.448 1 1 1h1c0.552 0 1-0.447 1-1s-0.448-1-1-1z" />
                                            <path d="m9 28.879h-1c-0.552 0-1 0.447-1 1s0.448 1 1 1h1c0.552 0 1-0.447 1-1s-0.448-1-1-1z" />
                                            <path d="m44.849 13.615-0.707 0.707c-0.391 0.391-0.391 1.023 0 1.414 0.195 0.195 0.451 0.293 0.707 0.293s0.512-0.098 0.707-0.293l0.707-0.707c0.391-0.391 0.391-1.023 0-1.414s-1.023-0.39-1.414 0z" />
                                            <path d="m14.444 44.021-0.707 0.707c-0.391 0.391-0.391 1.023 0 1.414 0.195 0.195 0.451 0.293 0.707 0.293s0.512-0.098 0.707-0.293l0.707-0.707c0.391-0.391 0.391-1.023 0-1.414s-1.024-0.39-1.414 0z" />
                                            <path d="m45.556 44.021c-0.391-0.391-1.023-0.391-1.414 0s-0.391 1.023 0 1.414l0.707 0.707c0.195 0.195 0.451 0.293 0.707 0.293s0.512-0.098 0.707-0.293c0.391-0.391 0.391-1.023 0-1.414l-0.707-0.707z" />
                                            <path d="m15.151 13.615c-0.391-0.391-1.023-0.391-1.414 0s-0.391 1.023 0 1.414l0.707 0.707c0.195 0.195 0.451 0.293 0.707 0.293s0.512-0.098 0.707-0.293c0.391-0.391 0.391-1.023 0-1.414l-0.707-0.707z" />
                                        </svg>
                                        <p class="info-icon-title">
                                            <?php echo $blog_to_show['date']; ?>
                                        </p>
                                    </div>
                                    <!-- <div class="inner-detail-wrapper">
                                        <svg enable-background="new 0 0 95.926 95.926" version="1.1" viewBox="0 0 95.926 95.926" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M47.893,47.221c11.768,0,21.341-10.592,21.341-23.611S59.66,0,47.893,0C36.125,0,26.55,10.592,26.55,23.61    C26.55,36.63,36.125,47.221,47.893,47.221z" />
                                            <path d="m72.477 44.123c-1.244-0.269-2.524 0.272-3.192 1.355-7.635 12.369-19.945 12.726-21.323 12.726s-13.687-0.357-21.32-12.722c-0.67-1.085-1.953-1.628-3.198-1.354-16.576 3.649-20.947 28.67-19.655 48.987 0.101 1.58 1.411 2.811 2.994 2.811h82.36c1.583 0 2.894-1.23 2.993-2.811 1.293-20.34-3.079-45.375-19.659-48.992z" />
                                        </svg>
                                        <p class="info-icon-title"></p>
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        <!-- Post Details ends here -->
                        <div class="content-div row">
                            <div class="content-div row">
                                <!-- social-media-icons starts here -->
                                <div class="blog-social-media-wrapper">
                                    <div class="blog-solial-media-icons">
                                        <a href="http://www.facebook.com/share.php?u=http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>&title=<?php echo $blog_to_show['title'] ?>" class="social-icon">
                                        <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="56.098" cy="56.098" r="56.098" fill="#3B5998" />
                            <path d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34   c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z" fill="#fff" />
                        </svg>
                                    </a>
                                        <a href="http://twitter.com/home?status=<?php echo $blog_to_show['title'] ?>+http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>" class="social-icon">
                                        <svg enable-background="new 0 0 112.197 112.197" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve">
                        <circle cx="56.099" cy="56.098" r="56.098" fill="#55ACEE" />
                        <path d="m90.461 40.316c-2.404 1.066-4.99 1.787-7.702 2.109 2.769-1.659 4.894-4.284 5.897-7.417-2.591 1.537-5.462 2.652-8.515 3.253-2.446-2.605-5.931-4.233-9.79-4.233-7.404 0-13.409 6.005-13.409 13.409 0 1.051 0.119 2.074 0.349 3.056-11.144-0.559-21.025-5.897-27.639-14.012-1.154 1.98-1.816 4.285-1.816 6.742 0 4.651 2.369 8.757 5.965 11.161-2.197-0.069-4.266-0.672-6.073-1.679-1e-3 0.057-1e-3 0.114-1e-3 0.17 0 6.497 4.624 11.916 10.757 13.147-1.124 0.308-2.311 0.471-3.532 0.471-0.866 0-1.705-0.083-2.523-0.239 1.706 5.326 6.657 9.203 12.526 9.312-4.59 3.597-10.371 5.74-16.655 5.74-1.08 0-2.15-0.063-3.197-0.188 5.931 3.806 12.981 6.025 20.553 6.025 24.664 0 38.152-20.432 38.152-38.153 0-0.581-0.013-1.16-0.039-1.734 2.622-1.89 4.895-4.251 6.692-6.94z" fill="#F1F2F2" />
                    </svg>
                                    </a>
                                        <!-- <a href="#" class="social-icon">
                                        <svg enable-background="new 0 0 89.758 89.758" class="insta-icon" version="1.1" viewBox="0 0 89.758 89.758" xml:space="preserve">
                        <g fill="#6A453B">
                            <path d="m58.255 23.88h-26.752c-4.27 0-7.744 3.474-7.744 7.744v26.752c0 4.27 3.474 7.745 7.744 7.745h26.752c4.27 0 7.745-3.474 7.745-7.745v-26.752c0-4.27-3.474-7.744-7.745-7.744zm-13.376 35.026c-7.667 0-13.905-6.238-13.905-13.906 0-7.667 6.238-13.905 13.905-13.905 7.668 0 13.906 6.238 13.906 13.905s-6.239 13.906-13.906 13.906zm14.353-24.936c-1.815 0-3.291-1.476-3.291-3.29s1.476-3.29 3.291-3.29c1.814 0 3.29 1.476 3.29 3.29s-1.476 3.29-3.29 3.29z" />
                            <path d="m44.879 36.971c-4.426 0-8.03 3.602-8.03 8.028 0 4.428 3.604 8.031 8.03 8.031 4.428 0 8.029-3.603 8.029-8.031 0-4.425-3.602-8.028-8.029-8.028z" />
                            <path d="m44.879 0c-24.785 0-44.879 20.094-44.879 44.879s20.094 44.879 44.879 44.879 44.879-20.094 44.879-44.879-20.094-44.879-44.879-44.879zm26.996 58.376c0 7.511-6.109 13.62-13.62 13.62h-26.752c-7.51 0-13.62-6.109-13.62-13.62v-26.752c0-7.51 6.11-13.62 13.62-13.62h26.752c7.511 0 13.62 6.11 13.62 13.62v26.752z" />
                        </g>
                    </svg>
                                    </a> -->
                                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>&title=<?php echo $blog_to_show['title'] ?>&source=http://sunalisclasses.com" class="social-icon">
                                        <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="56.098" cy="56.097" r="56.098" />
                        <path d="m89.616 60.611v23.128h-13.409v-21.578c0-5.418-1.936-9.118-6.791-9.118-3.705 0-5.906 2.491-6.878 4.903-0.353 0.862-0.444 2.059-0.444 3.268v22.524h-13.41s0.18-36.546 0-40.329h13.411v5.715c-0.027 0.045-0.065 0.089-0.089 0.132h0.089v-0.132c1.782-2.742 4.96-6.662 12.085-6.662 8.822 0 15.436 5.764 15.436 18.149zm-54.96-36.642c-4.587 0-7.588 3.011-7.588 6.967 0 3.872 2.914 6.97 7.412 6.97h0.087c4.677 0 7.585-3.098 7.585-6.97-0.089-3.956-2.908-6.967-7.496-6.967zm-6.791 59.77h13.405v-40.33h-13.405v40.33z" fill="#fff"/>
                    </svg>
                                    </a>

                                        <a href="https://plus.google.com/share?url=http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>" class="social-icon">
                                         <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve">
                        <circle cx="56.098" cy="56.097" r="56.098" fill="#DC4E41" />
                        <path d="m19.531 58.608c-0.199 9.652 6.449 18.863 15.594 21.867 8.614 2.894 19.205 0.729 24.937-6.648 4.185-5.169 5.136-12.06 4.683-18.498-7.377-0.066-14.754-0.044-22.12-0.033-0.012 2.628 0 5.246 0.011 7.874 4.417 0.122 8.835 0.066 13.252 0.155-1.115 3.821-3.655 7.377-7.51 8.757-7.443 3.28-16.94-1.005-19.282-8.813-2.827-7.477 1.801-16.5 9.442-18.675 4.738-1.667 9.619 0.21 13.673 2.673 2.054-1.922 3.976-3.976 5.864-6.052-4.606-3.854-10.525-6.217-16.61-5.698-11.939 0.142-22.387 11.164-21.934 23.091z" fill="#DC4E41" />
                        <path d="m79.102 48.668c-0.022 2.198-0.045 4.407-0.056 6.604-2.209 0.022-4.406 0.033-6.604 0.044v6.582c2.198 0.011 4.407 0.022 6.604 0.045 0.022 2.198 0.022 4.395 0.044 6.604 2.187 0 4.385-0.011 6.582 0 0.012-2.209 0.022-4.406 0.045-6.615 2.197-0.011 4.406-0.022 6.604-0.033v-6.582c-2.197-0.011-4.406-0.022-6.604-0.044-0.012-2.198-0.033-4.407-0.045-6.604-2.197-1e-3 -4.384-1e-3 -6.57-1e-3z" fill="#DC4E41" />
                        <g fill="#fff">
                            <path d="m19.531 58.608c-0.453-11.927 9.994-22.949 21.933-23.092 6.085-0.519 12.005 1.844 16.61 5.698-1.889 2.077-3.811 4.13-5.864 6.052-4.054-2.463-8.935-4.34-13.673-2.673-7.642 2.176-12.27 11.199-9.442 18.675 2.342 7.808 11.839 12.093 19.282 8.813 3.854-1.38 6.395-4.936 7.51-8.757-4.417-0.088-8.835-0.033-13.252-0.155-0.011-2.628-0.022-5.246-0.011-7.874 7.366-0.011 14.743-0.033 22.12 0.033 0.453 6.439-0.497 13.33-4.683 18.498-5.732 7.377-16.322 9.542-24.937 6.648-9.143-3.003-15.792-12.214-15.593-21.866z" />
                            <path d="m79.102 48.668h6.57c0.012 2.198 0.033 4.407 0.045 6.604 2.197 0.022 4.406 0.033 6.604 0.044v6.582c-2.197 0.011-4.406 0.022-6.604 0.033-0.022 2.209-0.033 4.406-0.045 6.615-2.197-0.011-4.396 0-6.582 0-0.021-2.209-0.021-4.406-0.044-6.604-2.197-0.023-4.406-0.033-6.604-0.045v-6.582c2.198-0.011 4.396-0.022 6.604-0.044 0.011-2.196 0.033-4.405 0.056-6.603z" />
                        </g></svg></a>

                                        <a href="https://web.whatsapp.com/send?text=<?php echo $blog_to_show['title']; ?> - http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>" class="social-icon">
                                      <svg enable-background="new 0 0 418.135 418.135" version="1.1" viewBox="0 0 418.14 418.14" xml:space="preserve">
                                        <g fill="#7AD06D">
	                                         <path d="m198.93 0.242c-110.43 5.258-197.57 97.224-197.24 207.78 0.102 33.672 8.231 65.454 22.571 93.536l-22.017 106.87c-1.191 5.781 4.023 10.843 9.766 9.483l104.72-24.811c26.905 13.402 57.125 21.143 89.108 21.631 112.87 1.724 206.98-87.897 210.5-200.72 3.771-120.94-96.047-219.55-217.41-213.77zm124.96 321.96c-30.669 30.669-71.446 47.559-114.82 47.559-25.396 0-49.71-5.698-72.269-16.935l-14.584-7.265-64.206 15.212 13.515-65.607-7.185-14.07c-11.711-22.935-17.649-47.736-17.649-73.713 0-43.373 16.89-84.149 47.559-114.82 30.395-30.395 71.837-47.56 114.82-47.56 43.372 1e-3 84.147 16.891 114.82 47.559 30.669 30.669 47.559 71.445 47.56 114.82-1e-3 42.986-17.166 84.428-47.561 114.82z"/>
	                                          <path d="m309.71 252.35-40.169-11.534c-5.281-1.516-10.968-0.018-14.816 3.903l-9.823 10.008c-4.142 4.22-10.427 5.576-15.909 3.358-19.002-7.69-58.974-43.23-69.182-61.007-2.945-5.128-2.458-11.539 1.158-16.218l8.576-11.095c3.36-4.347 4.069-10.185 1.847-15.21l-16.9-38.223c-4.048-9.155-15.747-11.82-23.39-5.356-11.211 9.482-24.513 23.891-26.13 39.854-2.851 28.144 9.219 63.622 54.862 106.22 52.73 49.215 94.956 55.717 122.45 49.057 15.594-3.777 28.056-18.919 35.921-31.317 5.362-8.453 1.128-19.679-8.494-22.442z"/>
                                          </g>
                                        </svg>
                                 </a>
                                    </div>
                                </div>
                                <!-- social-media-icons ends here -->

                                <!-- social-media-mobile-icons starts here -->
                                <div class="blog-social-media-mobile-wrapper">
                                    <div class="blog-solial-media-icons">
                                        <a href="http://www.facebook.com/share.php?u=http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>&title=<?php echo $blog_to_show['title']; ?>" class="social-icon">
                                        <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="56.098" cy="56.098" r="56.098" fill="#3B5998" />
                            <path d="M70.201,58.294h-10.01v36.672H45.025V58.294h-7.213V45.406h7.213v-8.34   c0-5.964,2.833-15.303,15.301-15.303L71.56,21.81v12.51h-8.151c-1.337,0-3.217,0.668-3.217,3.513v7.585h11.334L70.201,58.294z" fill="#fff" />
                        </svg>
                                    </a>
                                        <a href="http://twitter.com/home?status=<?php echo $blog_to_show['title']; ?>+http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>" class="social-icon">
                                        <svg enable-background="new 0 0 112.197 112.197" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve">
                        <circle cx="56.099" cy="56.098" r="56.098" fill="#55ACEE" />
                        <path d="m90.461 40.316c-2.404 1.066-4.99 1.787-7.702 2.109 2.769-1.659 4.894-4.284 5.897-7.417-2.591 1.537-5.462 2.652-8.515 3.253-2.446-2.605-5.931-4.233-9.79-4.233-7.404 0-13.409 6.005-13.409 13.409 0 1.051 0.119 2.074 0.349 3.056-11.144-0.559-21.025-5.897-27.639-14.012-1.154 1.98-1.816 4.285-1.816 6.742 0 4.651 2.369 8.757 5.965 11.161-2.197-0.069-4.266-0.672-6.073-1.679-1e-3 0.057-1e-3 0.114-1e-3 0.17 0 6.497 4.624 11.916 10.757 13.147-1.124 0.308-2.311 0.471-3.532 0.471-0.866 0-1.705-0.083-2.523-0.239 1.706 5.326 6.657 9.203 12.526 9.312-4.59 3.597-10.371 5.74-16.655 5.74-1.08 0-2.15-0.063-3.197-0.188 5.931 3.806 12.981 6.025 20.553 6.025 24.664 0 38.152-20.432 38.152-38.153 0-0.581-0.013-1.16-0.039-1.734 2.622-1.89 4.895-4.251 6.692-6.94z" fill="#F1F2F2" />
                    </svg>
                                    </a>

                                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.sunalisclasses.com/blog/<?php echo  $blog_to_show['url'] ?>&title=<?php echo $blog_to_show['title']; ?>&source=http://sunalisclasses.com" class="social-icon">
                                        <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve">
                        <circle cx="56.098" cy="56.097" r="56.098" />
                        <path d="m89.616 60.611v23.128h-13.409v-21.578c0-5.418-1.936-9.118-6.791-9.118-3.705 0-5.906 2.491-6.878 4.903-0.353 0.862-0.444 2.059-0.444 3.268v22.524h-13.41s0.18-36.546 0-40.329h13.411v5.715c-0.027 0.045-0.065 0.089-0.089 0.132h0.089v-0.132c1.782-2.742 4.96-6.662 12.085-6.662 8.822 0 15.436 5.764 15.436 18.149zm-54.96-36.642c-4.587 0-7.588 3.011-7.588 6.967 0 3.872 2.914 6.97 7.412 6.97h0.087c4.677 0 7.585-3.098 7.585-6.97-0.089-3.956-2.908-6.967-7.496-6.967zm-6.791 59.77h13.405v-40.33h-13.405v40.33z" fill="#fff"/>
                    </svg>
                                    </a>
                                        <a href="https://plus.google.com/share?url=http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>" class="social-icon">
                                         <svg enable-background="new 0 0 112.196 112.196" version="1.1" viewBox="0 0 112.2 112.2" xml:space="preserve">
                        <circle cx="56.098" cy="56.097" r="56.098" fill="#DC4E41" />
                        <path d="m19.531 58.608c-0.199 9.652 6.449 18.863 15.594 21.867 8.614 2.894 19.205 0.729 24.937-6.648 4.185-5.169 5.136-12.06 4.683-18.498-7.377-0.066-14.754-0.044-22.12-0.033-0.012 2.628 0 5.246 0.011 7.874 4.417 0.122 8.835 0.066 13.252 0.155-1.115 3.821-3.655 7.377-7.51 8.757-7.443 3.28-16.94-1.005-19.282-8.813-2.827-7.477 1.801-16.5 9.442-18.675 4.738-1.667 9.619 0.21 13.673 2.673 2.054-1.922 3.976-3.976 5.864-6.052-4.606-3.854-10.525-6.217-16.61-5.698-11.939 0.142-22.387 11.164-21.934 23.091z" fill="#DC4E41" />
                        <path d="m79.102 48.668c-0.022 2.198-0.045 4.407-0.056 6.604-2.209 0.022-4.406 0.033-6.604 0.044v6.582c2.198 0.011 4.407 0.022 6.604 0.045 0.022 2.198 0.022 4.395 0.044 6.604 2.187 0 4.385-0.011 6.582 0 0.012-2.209 0.022-4.406 0.045-6.615 2.197-0.011 4.406-0.022 6.604-0.033v-6.582c-2.197-0.011-4.406-0.022-6.604-0.044-0.012-2.198-0.033-4.407-0.045-6.604-2.197-1e-3 -4.384-1e-3 -6.57-1e-3z" fill="#DC4E41" />
                        <g fill="#fff">
                            <path d="m19.531 58.608c-0.453-11.927 9.994-22.949 21.933-23.092 6.085-0.519 12.005 1.844 16.61 5.698-1.889 2.077-3.811 4.13-5.864 6.052-4.054-2.463-8.935-4.34-13.673-2.673-7.642 2.176-12.27 11.199-9.442 18.675 2.342 7.808 11.839 12.093 19.282 8.813 3.854-1.38 6.395-4.936 7.51-8.757-4.417-0.088-8.835-0.033-13.252-0.155-0.011-2.628-0.022-5.246-0.011-7.874 7.366-0.011 14.743-0.033 22.12 0.033 0.453 6.439-0.497 13.33-4.683 18.498-5.732 7.377-16.322 9.542-24.937 6.648-9.143-3.003-15.792-12.214-15.593-21.866z" />
                            <path d="m79.102 48.668h6.57c0.012 2.198 0.033 4.407 0.045 6.604 2.197 0.022 4.406 0.033 6.604 0.044v6.582c-2.197 0.011-4.406 0.022-6.604 0.033-0.022 2.209-0.033 4.406-0.045 6.615-2.197-0.011-4.396 0-6.582 0-0.021-2.209-0.021-4.406-0.044-6.604-2.197-0.023-4.406-0.033-6.604-0.045v-6.582c2.198-0.011 4.396-0.022 6.604-0.044 0.011-2.196 0.033-4.405 0.056-6.603z" />
                        </g></svg></a>
                                        <a href="whatsapp://send?text=http://www.sunalisclasses.com/blog/<?php echo $blog_to_show['url'] ?>" class="social-icon">
                                      <svg enable-background="new 0 0 418.135 418.135" version="1.1" viewBox="0 0 418.14 418.14" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                        <g fill="#7AD06D">
                                           <path d="m198.93 0.242c-110.43 5.258-197.57 97.224-197.24 207.78 0.102 33.672 8.231 65.454 22.571 93.536l-22.017 106.87c-1.191 5.781 4.023 10.843 9.766 9.483l104.72-24.811c26.905 13.402 57.125 21.143 89.108 21.631 112.87 1.724 206.98-87.897 210.5-200.72 3.771-120.94-96.047-219.55-217.41-213.77zm124.96 321.96c-30.669 30.669-71.446 47.559-114.82 47.559-25.396 0-49.71-5.698-72.269-16.935l-14.584-7.265-64.206 15.212 13.515-65.607-7.185-14.07c-11.711-22.935-17.649-47.736-17.649-73.713 0-43.373 16.89-84.149 47.559-114.82 30.395-30.395 71.837-47.56 114.82-47.56 43.372 1e-3 84.147 16.891 114.82 47.559 30.669 30.669 47.559 71.445 47.56 114.82-1e-3 42.986-17.166 84.428-47.561 114.82z"/>
                                            <path d="m309.71 252.35-40.169-11.534c-5.281-1.516-10.968-0.018-14.816 3.903l-9.823 10.008c-4.142 4.22-10.427 5.576-15.909 3.358-19.002-7.69-58.974-43.23-69.182-61.007-2.945-5.128-2.458-11.539 1.158-16.218l8.576-11.095c3.36-4.347 4.069-10.185 1.847-15.21l-16.9-38.223c-4.048-9.155-15.747-11.82-23.39-5.356-11.211 9.482-24.513 23.891-26.13 39.854-2.851 28.144 9.219 63.622 54.862 106.22 52.73 49.215 94.956 55.717 122.45 49.057 15.594-3.777 28.056-18.919 35.921-31.317 5.362-8.453 1.128-19.679-8.494-22.442z"/>
                                          </g>
                                        </svg>
                                 </a>
                                    </div>
                                </div>


                                <!-- social-media-mobile-icons ends here -->
                                <div class="col content-text">


                                    <!-- social-media-icons ends here -->

                                    <div class="col content-text" id="blog-content">
                                        <?php echo $blog_to_show["blog"]; ?>
                                    </div>
                                    <!-- content-text ends here -->
                                </div>
                                <!-- Content div ends here -->
                            </div>
                            <!-- Content area ends here -->



                            <div class="fbcomment-div">
                                <div class="fb-comments" data-href="http://sunalisclasses.com/fbcomments/blog/<?php echo $url_title; ?>" data-width="1000px" data-numposts="5"></div>
                                <div id="fb-root"></div>
                                <script>
                                    (function(d, s, id) {
                                        console.log(id);
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=153203914724487&autoLogAppEvents=1';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));

                                </script>
                            </div>
                            <!-- FB Comments ends here -->



                        </div>
                        <!-- Column1 ends here -->





                    </div>


                </div>
                <div class="column2 col-lg-3">
                    <div class="sidebar">

                        <div class="recent-posts-wrapper">
                            <h3>Recent Post</h3>
                            <hr>
                            <?php foreach($blogs_data as $key=>$value): ?>
                            <a href="http://localhost/sunalisclasses/blog/<?php echo $value['url']; ?>">
                                <p>
                                    <?php echo $value['title']; ?>
                                </p>
                            </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>





    <footer class="sunalis-footer">
        <div class="blog-container">
            <div class="row">
                <div class="col-md-6">
                    <p class="copyright">&copy; 2017, Sunali's Classes</p>
                </div>
                <div class="col-md-6">
                    <p class="pixolo">Made with &#9829; by Pixolo Webworks</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="http://sunalisclasses.com/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://sunalisclasses.com/vendor/jquery-easing/jquery.easing.min.js"></script>






</body>

</html>
